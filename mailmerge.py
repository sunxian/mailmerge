'''MAILMERGE
By Dirk Colbry

Minimum working Code

import pandas as pd
import mailmerge as mm

pf = pd.DataFrame([['Dirk', 'Colbry', 'colbrydi@msu.edu']], columns=['First', 'Last', 'Email'])

template = mm.init_message(Subject="Hello World", 
                           To='colbrydi@msu.edu', 
                           From='<<Email>>', 
                           Message='Hello <<First>>, This is a test. - Dirk')

mm.sendmessages(template, pf, sendtest=False)

'''

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import markdown
from IPython.core.display import HTML,display
import pandas as pd
import math
import time
from datetime import datetime
from pathlib import Path,PurePath
import re


def outlookcontacts2df(textstring, deliminator="\n"):
    df = pd.DataFrame()
    mylist = textstring.split(deliminator)
    for row in mylist:
        entry = dict()
        if ',' in row:
            rowlist = row.split(',')
            entry['Last'] = rowlist[0].strip()
            entry['First']  = rowlist[1].split('<')[0].strip()
        else:
            rowlist = row.split(' ')
            entry['First'] = rowlist[0].strip()
            entry['Last'] = rowlist[1].strip()
        entry['Email'] = row.split('<')[1][:-1].strip()
        #print(f"AFTER - {entry}")
        df = pd.concat([df, pd.DataFrame([entry])], ignore_index=True)
    df = df.drop_duplicates('Email')
    df.sort_values('First', inplace=True)
    return df


def send_check():
    #Turn on testing to make sure everything is working
    sendtest=True
    x = input('Type \"SEND\" (without the quotes) to send the message:')
    if x == "SEND":
        sendtest=False

    if(sendtest):
        print("TESTING Only, Emails will NOT be sent")
    else:
        for i in range(0,20):
            print("WARNING, DANGER!!!!, IF you continue, Emails will be Sent")
    return sendtest


def init_message(Subject='', Cc='', Bcc='', To='', From='', Message=''):
    template=dict()
    template['Subject']=Subject
    template['Cc']=Cc
    template['Bcc']=Bcc
    template['To']=To
    template['From']=From 
    template['Attachments']=''
    template['Message']=Message
    return template

def tabs2df(data):
    stringList = data.split('\n')
    columns = stringList[0].split('\t')
    stringList  = stringList[1:]
    df = pd.DataFrame([data.split('\t') for data in stringList], columns=columns)
    return df


def preview(template, df, test=True):
    sendmessages(template, df, test=test)


def loopsend(template, df, maxsend=750, start=0, duration=(60*60)+10, server='smtp.egr.msu.edu', test = True):
    '''Send a message on a loop to get around maxsend limits.
    This will send ```maxsend``` numbers of emais each ```duration``` seconds'''
    while(start <= len(df)):
        end = start+maxsend
        if (end > len(df)):
            end = len(df)
        print(f"From {start} to {end} of {len(df)}")
        block = df.iloc[start:end, :]
        sendmessages(template, block, server=server, test = test)
        print(f"Just Finished {start}-{end} of {len(df)}, Sleeping for {duration} seconds starting {datetime.now()}")
        if not end > len(df):
            print(f"Sleeping {duration} seconds")
            time.sleep(duration)
        start = end


def sendmail(new_message, server='smtp.egr.msu.edu', test = True):
    
    new_message['MD_message'] = markdown.markdown(new_message['Message'])

    msg = MIMEMultipart('alternative')
    part1 = MIMEText(new_message['Message'], 'plain')
    part2 = MIMEText(new_message['MD_message'], 'html')
    msg['Subject'] = new_message['Subject']
    msg['From'] = new_message['From']
    msg['To'] = new_message['To']
    msg.attach(part1)
    msg.attach(part2)

    rcpt = new_message['To'].split(",")
    if new_message['Cc']:
        rcpt = new_message['Cc'].split(",") + rcpt
        msg['Cc'] = new_message['Cc']
    if new_message['Bcc']:
        rcpt = new_message['Bcc'].split(",") + rcpt
        msg['Bcc'] = new_message['Bcc']

    if new_message['Attachments'] == '':
        files = []        
    else:
        files = new_message['Attachments'].split(',')
    
    for file in files:
        path = Path(file)
        if (path.is_file()):
            with open(path, "rb") as f:
                attach = MIMEApplication(f.read(),_subtype="pdf")
                attach.add_header('Content-Disposition','attachment',filename=str(PurePath(file).name))
            msg.attach(attach)
        else:
            raise Exception(f'ERROR: File {file} not found!')
        
    #print(rcpt)
    if test:
        print('To:',new_message['To'])
        print('From:',new_message['From'])
        if new_message['Cc']:
            print('CC:',new_message['Cc'])
        if new_message['Bcc']:
            print('BCC:', new_message['Bcc'])
        print('Subject:', new_message['Subject'])
        print('Attachments:', new_message['Attachments'])
        display(HTML(new_message['MD_message']))
    else:
        try:
            # Send the message via engineering SMTP server.
            print('Connecting to SMTP and sending message to', new_message['To'])
            keep_server_open = True
            if not type(server) == smtplib.SMTP:
                server = smtplib.SMTP(server)
                keep_server_open = False

            server.sendmail(new_message['From'], rcpt, msg.as_string())
            if not keep_server_open:
                server.quit()

        except SMTPException:
            print("Error: unable to send email to", new_message['To'])


def sendmessages(template, df, server='smtp.egr.msu.edu', sendtest = ""):
   
    if sendtest == "":
        sendtest = send_check()

    if not sendtest:
        server = smtplib.SMTP(server)
    tags = list(df.columns) # Using columns as tags
    emailcount=0
    for index,dfrow in df.iterrows():
        emailcount=emailcount+1
        new_message = template.copy()
        print("========================================================================")

        for component in new_message:
            for j,tag in enumerate(tags):
                new_message[component] = new_message[component].replace(f'<<{tag}>>', f"{dfrow[tag]}")
            if "<<" in new_message[component]:
                tags = re.findall('<<.*>>', new_message[component])
                raise Exception(f"ERROR: tags {tags} still found in {component}")
        sendmail(new_message, server=server, test=sendtest)
    if not sendtest:
        server.quit()

