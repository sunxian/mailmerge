# Simple Mailmerge program

Written by Dirk Colbry

Welcome to Dirk's simple mailmerge program.  I use this to improve a lot of my email workflows from sending feedback to students to emailing different groups weekly reports.  I like that it just uses a pandas dataframe. Here are the basic steps to use the program.

1. Create a pandas dataframe with each row representing an email you want to send and each column representing the things you would like to be different in each email. Typically there is an Email Address column and maybe a Name column but you can add anything you want.
2. Create an eamil "template" using the library. THe template consists of some common email feilds. You want to fill out each feild as a string.  You include \<\<TAG\>\> syntax where the TAG is the name of a column in your pandas dataframe.  This is what will get replaced when you send each email.
3. Run the sendmail function using the template and the dataframe.  It is easy as that. 

This notebook shows you a basic template for how I use the tool. I highly recommend including the sendtest option so you can test and make sure everything is working before you actually send the email. 

**_NOTE_**: This is setup primarily to work on the engineering jupyter server at Michigan State University. These computers already have SMTP servers set up when we log in so there is very little configuration required.  The code has not been tested on a local machine using other SMTP servers so there will probabably require additional authentication steps. 


Please see the [Example.ipynb](Example.ipynb) file for detailed instructions and an example
